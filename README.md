# MPMA

An experimental mobile client for MSPFA.

-   Dark mode out-of-the-box
-   Optimized for mobile
-   Auto-color-dulling
-   Adventure saves
-   ...all without an account

## Setup

```
$ pnpm i
```

## Development

```
$ pnpm dev
```

## Build (for Android)

```
$ pnpm build
$ pnpm sync
$ npx cap open android
```

Then click the little green hammer in Android Studio.
APK is at `android/app/build/apk/debug/app-debug.apk`

## Manifesto

-   UX over everything
    -   Ads detract from an experience
-   Make the majority better, rather than supporting the all
-   Web technologies are one honking great idea -- let's do more of those!
