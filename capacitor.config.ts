import { CapacitorConfig } from "@capacitor/cli";

const config: CapacitorConfig = {
	appId: "me.yhvr.mpma",
	appName: "MPMA",
	webDir: "dist",
	bundledWebRuntime: false,
};

export default config;
