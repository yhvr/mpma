import { RGBColor } from "./rgbcolor";

let textColor = "#eeeeee";

export function setTextColor(newColor: string) {
	textColor = newColor;
}

const weight = 2;
const total = weight + 1;
function blendColors(base, delta) {
	// XXX
	let basec = new RGBColor(base);
	let deltac = new RGBColor(delta);
	let newR = Math.floor(Math.abs(basec.r * weight + deltac.r) / total);
	let newG = Math.floor(Math.abs(basec.g * weight + deltac.g) / total);
	let newB = Math.floor(Math.abs(basec.b * weight + deltac.b) / total);
	let newStr = `rgb(${newR}, ${newG}, ${newB})`;
	return newStr;
}

export function triggerBlend() {
	document.querySelectorAll("span[toblend]").forEach(el => {
		// @ts-expect-error
		let blendTo = blendColors(textColor, el.attributes.toblend.nodeValue);
		// @ts-expect-error
		el.style.color = blendTo;
	});
}
