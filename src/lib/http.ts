export async function post(path: string, data: { [key: string]: any }) {
	data.do = path;

	let req = await fetch("https://mspfa.com/", {
		method: "POST",
		body: new URLSearchParams(data).toString(),
		headers: {
			"Content-Type": "application/x-www-form-urlencoded",
		},
	}).catch(e => console.error(e));

	if (!(req instanceof Response)) return {};
	let out = await req.json();

	return out;
}

export interface StoriesQuery {
	count: number;
	name?: string; // n
	tags?: string; // t
	p?: string; // ??
	order: "updated" | "favs" | "random"; // o
	offset?: number; // f (optional)
	active: ("inactive" | "ongoing" | "complete")[]; // h
	// Inactive: +2
	// Ongoing: +4
	// Complete: +8
}

// enum res.h:
//    1: Inactive
//    2: Ongoing
//    3: Complete
export async function stories(q: StoriesQuery) {
	let activitySum = 0;
	if (q.active.includes("inactive")) activitySum += 2;
	if (q.active.includes("ongoing")) activitySum += 4;
	if (q.active.includes("complete")) activitySum += 8;
	if (activitySum === 0) alert("Warning: activitySum == 0! (/lib/http.ts)");

	return await post("stories", {
		m: q.count,
		n: q.name ?? "",
		t: q.tags ?? "",
		p: "p",
		o: q.order,
		h: activitySum,
	});
}

export async function story(story: number) {
	return await post("story", { s: story });
}